# cyber_evening

Few hours electronics projects. Made from rubbish, because electronics is everywhere and gets in the way under your feet.\
Tested devices are with photo.
Devices list:

1. lamp with door sensor. Functions:\
*300mA li-ion charging circuit with charge status indicator.\
*Reed sensor for switching on by the door.\
*0.7W output power.\
![](emergency_lamp/emergency_lamp_readme.png)
![](emergency_lamp/emergency_lamp_3d.png)
![](emergency_lamp/emergency_lamp_smd_back.png)
![](emergency_lamp/S4120011.JPG)
2. LM4766 power amplifier replacement module for vega\
![](lm4766_module/lm4766_module_readme.png)
![](lm4766_module/lm4766_module_3d.png)
3. Cheap flashlight DC-DC boost converter for stable brightness during voltage drop of discharging battery 
plus 4.6V stabilizer for ni-mh charger.\
![](nimh_flashlight/nimh_flashlight%20.png)
![](nimh_flashlight/nimh_flashlight_3d%20.png)
![](nimh_flashlight/S4210002.JPG)
4. High-effiency flashlight driver with Ni-Mh battery. Specs:\ 
*Charge controller with USB in.\
*Two mode button switch.\
*Up to 700mA current.\
![](nimh_flashlight2/nimh_flashlight2_3d.png)
![](nimh_flashlight2/nimh_flashlight2_small.png)
5. High-effiency flashlight driver with Li-ion battery. Specs:\ 
*Charge controller with micro USB input.\
*Two mode button switch.\
*Up to 2A current.\
![](liion_flashlight2/liion_flashlight2_3d.png)\
![](liion_flashlight2/liion_flashlight2_small.png)
6. Camping lamp (li-ion flashlight) with 6V leds. 5.7W output power.\
![](liion_flashlight4/liion_flashlight4_3d.png)
![](liion_flashlight4/liion_flashlight4_small.png)
7. DC-DC power bank with flashlight driver. 1A power bank output, 1A LED.\
![](liion_flashlight3/liion_flashlight3_3d.png)
![](liion_flashlight3/liion_flashlight3_small.png)
8. Primitive flashlight with 220V charger.\
![](ac_flashlight/ac_flashlight_photo.JPG)
![](ac_flashlight/ac_flashlight3d.png)
![](ac_flashlight/ac_flashlight_small.png)
9. Cheapest 220V led projector. Made in the package from doshirak noodles. Specs:\
![](doshirak_projector/doshirak_projector_3d.png)
![](doshirak_projector/doshirak_projector_small.png)
10. Portable lamp for the car repair. 14W led module, based on 36V leds with linear driver.\
![](portable_lamp/portable_lamp_3d.png)
![](portable_lamp/portable_lamp_small.png)
11. 24V 0.7A heater power supply with overcurrent protection and temperature control.\
![](heater_supply/heater_supply3d.png)
![](heater_supply/heater_supply_small.png)
